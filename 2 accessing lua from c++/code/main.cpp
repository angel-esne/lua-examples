
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2015.05+

#include <iostream>
#include <LuaState.h>

using std::cin;
using std::cout;
using std::endl;

int main ()
{
    lua::State state;

    // Accessing a global variable:

    state.doString ("v = 0");

    state.set ("v", 100);

    cout << "The value of v is " << int(state["v"]) << endl;

    // Calling a Lua function:

    state.doString
    (
        "function sayHello (name)"
        "    print ('Hello '..name..'!')"
        "end"
    );

    state["sayHello"] ("developer");

    // Getting the return value of a function:

    state.doString
    (
        "function min (a, b) \n"
        "    if a < b then return a else return b end \n"
        "end \n"
    );

    cout << "min (5, 10) returns " << state["min"] (5, 10).to< int > () << endl;

    cin.get ();

    return 0;
}
