
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2015.05+

#include <iostream>

extern "C"
{
    #include <lua.h>
    #include <lualib.h>
    #include <lauxlib.h>
}

int main ()
{
    lua_State * lua_state = luaL_newstate ();

    luaL_openlibs (lua_state);
    luaL_dostring (lua_state, "print ('Hello, world!')");
    lua_close     (lua_state);

    std::cin.get ();

    return 0;
}
