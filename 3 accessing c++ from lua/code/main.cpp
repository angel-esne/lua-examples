
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2015.05+

#include <iostream>
#include <functional>
#include <LuaState.h>

namespace
{

    void my_c_print_function (const char * message)
    {
        std::cout << "Message through my_c_print_function(): " << message << std::endl;
    }

    class My_Class
    {

        const char * prefix;

    public:

        My_Class(const char * given_prefix)
        :
            prefix(given_prefix)
        {
        }

        void print (const char * message)
        {
            std::cout << prefix << message << std::endl;
        }

        void callback ()
        {
            std::cout << "Object callback called." << std::endl;
        }

    };

}

int main ()
{
    lua::State lua_state;

    My_Class my_object("Message through My_Class::print(): ");

    lua_state.set ("c_print"   , &my_c_print_function);
    lua_state.set ("cpp_print" , [&my_object] (const char * message) { my_object.print (message); });
    lua_state.set ("callback"  , std::function< void() > (std::bind (&My_Class::callback, my_object)));
    lua_state.set ("lambda_mul", [] (int a, int b) -> int { return a * b; });

    lua_state.doString ("c_print   ('Hello from Lua!')");
    lua_state.doString ("cpp_print ('Hello object!')");
    lua_state.doString ("print     ('Product from C++: ' .. lambda_mul (5, 25))" );
    lua_state.doString ("callback  ()");

    std::cin.get ();

    return 0;
}
