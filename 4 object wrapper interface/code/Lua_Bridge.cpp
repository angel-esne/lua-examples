
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05+

#include "Lua_Bridge.hpp"

namespace example
{

	void Lua_Bridge::initialize_game_object_interface ()
	{
		lua_state.set
		(
			"get_preexisting_game_object",
			[this] ()
			{
				return create_game_object_table (preexisting_game_object);
			}
		);

		lua_state.set
		(
			"create_game_object",
			[this] (const char * given_name)
			{
				return create_game_object_table (given_name);
			}
		);
	}

	lua::Value Lua_Bridge::create_new_empty_table ()
	{
		lua_state.set ("__new_empty_table", lua::Table());

		lua::Value table = lua_state["__new_empty_table"];

		lua_state.set ("__new_empty_table", nullptr);

		return table;
	}

	Game_Object * Lua_Bridge::get_game_object_pointer (lua::Value table)
	{
		if (table.is< lua::Table > ())
		{
			auto instance = table["instance"];

			if  (instance.is< lua::Pointer >())
			{
				return reinterpret_cast< Game_Object * >(instance.to< lua::Pointer > ());
			}
		}

		return nullptr;
	}

	lua::Value Lua_Bridge::create_game_object_table (const std::string & given_name)
	{
		auto game_object = std::make_shared< Game_Object > (given_name);

		return create_game_object_table (game_object);
	}

	lua::Value Lua_Bridge::create_game_object_table (std::shared_ptr< Game_Object > & game_object)
	{
		game_object_instances.push_back (game_object);

		auto value = create_new_empty_table ();

		value.set ("instance", lua::Pointer(game_object.get ()));

		value.set ("getName", [this] (lua::Value table)          { auto self = get_game_object_pointer (table); return self ? self->get_name () : std::string(); });
		value.set ("getX",    [this] (lua::Value table)          { auto self = get_game_object_pointer (table); return self ? self->get_x    () : 0.f; });
		value.set ("getY",    [this] (lua::Value table)          { auto self = get_game_object_pointer (table); return self ? self->get_y    () : 0.f; });
		value.set ("getZ",    [this] (lua::Value table)          { auto self = get_game_object_pointer (table); return self ? self->get_z    () : 0.f; });
		value.set ("setX",    [this] (lua::Value table, float x) { auto self = get_game_object_pointer (table); if (self) self->set_x (x); });
		value.set ("setY",    [this] (lua::Value table, float y) { auto self = get_game_object_pointer (table); if (self) self->set_y (y); });
		value.set ("setZ",    [this] (lua::Value table, float z) { auto self = get_game_object_pointer (table); if (self) self->set_z (z); });

		return value;
	}

}
