
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05+

#include <iostream>
#include "Lua_Bridge.hpp"

using namespace example;

int main ()
{
    Lua_Bridge lua_bridge;

    lua_bridge.execute 
    (
        "a = create_game_object ('lua-game-object')"
        "b = get_preexisting_game_object ()"

        "a:setX (10)"
        "a:setY (11)"
        "a:setZ (12)"

        "print ('New GameObject name: ' .. a:getName())"
        "print ('New GameObject position: ' .. a:getX() .. ', ' .. a:getY() .. ', ' .. a:getZ())"
        "print ('Preexisting GameObject name: ' .. b:getName ())"
        "print ('Preexisting GameObject position: ' .. b:getX() .. ', ' .. b:getY() .. ', ' .. b:getZ())"
    );

    std::cin.get ();

    return 0;
}
