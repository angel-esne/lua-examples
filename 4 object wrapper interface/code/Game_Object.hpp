
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05+

#pragma once

#include <string>

namespace example
{

    class Game_Object
    {

        std::string name;

        float x, y, z;

    public:

        Game_Object(const std::string & given_name)
        :
            name(given_name)
        {
            x = y = z = 0.f;
        }

        std::string get_name () const
        {
            return name;
        }

        float get_x () const { return x; }
        float get_y () const { return y; }
        float get_z () const { return z; }

        void  set_x (float new_x) { x = new_x; }
        void  set_y (float new_y) { y = new_y; }
        void  set_z (float new_z) { z = new_z; }

    };

}
