
// Este c�digo es de dominio p�blico.
// angel.rodriguez@esne.edu
// 2021.05+

#pragma once

#include <memory>
#include <string>
#include <vector>

#include <LuaState.h>
#include "Game_Object.hpp"

namespace example
{

	class Lua_Bridge
	{
		
		lua::State lua_state;

        std::vector< std::shared_ptr< Game_Object > > game_object_instances;

		std::shared_ptr< Game_Object > preexisting_game_object;

	public:

		Lua_Bridge()
		{
			preexisting_game_object = std::make_shared< Game_Object >("this_game_object_is_always_available");

			build_bridge ();
		}

		lua::Value execute(const char * code)
		{
			return lua_state.doString  (code);
		}

	private:

		void build_bridge ()
		{
			initialize_game_object_interface ();
		}

		void initialize_game_object_interface ();

		lua::Value create_new_empty_table();

	private:

		static Game_Object * get_game_object_pointer (lua::Value table);

		lua::Value create_game_object_table (const std::string & given_name);
		lua::Value create_game_object_table (std::shared_ptr< Game_Object > & game_object);

	};

}